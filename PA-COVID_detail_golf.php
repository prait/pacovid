<?
include("include/authen.php");
include("include/misc.php");

$conn = connect_db();

$policy_id = GetRequest("policy_id");
$mode = GetRequest("mode");

$sql = "SELECT * FROM scil_webservice.MAS_APPLICATION";
$sql .= " WHERE tempQuotationNo = '" .$policy_id . "'";
//echo $sql . "<BR>";
$result = mysql_query($sql);
if ($rs = mysql_fetch_array($result)) {
	$cust_id = $rs["tempIcCard"];
	$comdate = $rs["tempEffectDt"];
	$expdate = $rs["tempExpiryDt"];
	$title = $rs["tempTitleNm"];
	$fname = $rs["tempFirstNm"];
	$lname = $rs["tempLastNm"];
	$bdate = $rs["tempLifeBirth"];
	$email = $rs["tempEmail"];
	$tel = $rs["tempTelNo"];
	$addr = $rs["tempAddrNo"];
	$addr2 = $rs["tempBld"];
	$province = $rs["tempChwNm"];
	$district = $rs["tempAmpNm"];
	$sub_district = $rs["tempThmNm"];

	$province_name = $rs["tempLifeChangewat"];
	$district_name = $rs["tempLifeAmphur"];
	$sub_district_name = $rs["tempLifeTambol"];

	$post = $rs["tempPostcode"];
	$benefit = $rs["BENEFIT_FLAG"];
	$relation = $rs["tempRelation"];
	$btitle = $rs["tempTitleBeneNm"];
	$bfname = $rs["tempFirstBeneNm"];
	$blname = $rs["tempLastBeneNm"];
	$pkg_id = $rs["tempFormula"];
}

$sql = "SELECT * FROM scil.MAS_PACKAGE2";
$sql .= " WHERE PKG_ID = '" . $pkg_id. "'";
//echo $sql . "<BR>";
$result = mysql_query($sql);
if ($rs = mysql_fetch_array($result)) {
	$pkg_name = $rs["PKG_NAME"];
	$net_prem = $rs["NET_PREM"];
	$stamp = $rs["STAMP"];
	//$tax = $rs["TAX"];
	$tax = round((($net_prem+$stamp)*($rs["TAX"]/100)),2);
	$total = round($net_prem + $stamp + $tax, 2);
	$sum_insure = $rs["SUM_INSURE"];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>FWD | PA & COVID-19</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-orange navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      <li class="nav-item">
        <div class="navbar-search-block">
          <form class="form-inline">
            <div class="input-group input-group-sm">
              <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
              <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                  <i class="fas fa-search"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-warning elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
		<img src="images/fwd_logo.png" alt="FWD" class="brand-image img-circle elevation-3" style="opacity: .8">
		<span class="brand-text font-weight-light">Policy Online</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
		<div class="image">
			<a href="#" class="d-block"><i class="fas fa-user"></i></a>
		</div>
        <div class="info">		  
          <a href="#" class="d-block"><?=$_SESSION["ADMIN_NAME"]?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
		  <li class="nav-item menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-shield-alt"></i>
              <p>
                ประกันอุบัติเหตุและโควิค
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="PA-COVID_list.php" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>รายการกรมธรรม์</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="promocode_list.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>โปรโมชั่นโค้ด</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1>รายการประกันอุบัติเหตุและโควิค</h1>
		  </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
			  <div class="card-body">
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label>หมายเลขบัตรประชาชน</label>
							<p><?=$cust_id?></p>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
						  <label>วันที่เริ่มคุ้มครอง</label>
						  <p><?=$comdate?></p>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
						  <label>วันที่สิ้นสุดคุ้มครอง</label>
						  <p><?=$expdate?></p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
						  <label>คำนำหน้า</label>
						  <p><?=$title?></p>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<label>ชื่อ</label>
							<p><?=$fname?></p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>นามสกุล</label>
							<P><?=$lname?></P>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
						  <label>วันเกิด</label>
							<P><?=PrintDate($bdate)?></P>
						</div>
					</div>				
					<div class="col-md-5">
						<div class="form-group">
							<label>อีเมล์</label>
							<P><?=$email?></P>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>หมายเลขโทรศัพท์</label>
							<P><?=$tel?></P>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>ที่อยู่</label>
							<P><?=$addr?></P>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>อาคาร / หมู่บ้าน, ซอย, ถนน</label>
							<P><?=$addr2?></P>
						</div>
					</div>
				</div>
				<div class="row">
				  <div class="col-md-3">
					<div class="form-group">
					  <label>จังหวัด</label>
					  <P><?=$province_name?></P>
					</div>
				  </div>
				   <div class="col-md-3">
					<div class="form-group">
					  <label>อำเภอ</label>
					  <P><?=$district_name?></P>
					</div>
				  </div>
				   <div class="col-md-3">
					<div class="form-group">
					  <label>ตำบล</label>
					  <P><?=$sub_district_name?></P>
					</div>
				  </div>
				  <div class="col-md-3">
					<div class="form-group">
					  <label>รหัสไปรษณีย์</label>
					  <P><?=$post?></P>
					</div>
				  </div>
				  <!-- /.col -->
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>ผู้รับผลประโยชน์</label>
							<div class="custom-control custom-radio">
							  <i class="fa fa-check"></i> <?=$relation?>
							</div>
						</div>
					</div>
				</div>
				<? if (($relation != "") && ($relation != "ทายาทตามกฎหมาย") && ($relation != "ทายาทโดยธรรม") && ($bfname != "")) { ?>
				<div class="row" id="benefit_spec">
					<div class="col-md-3">
						<div class="form-group">
						  <label>ความสัมพันธ์</label>
						  <P><?=$relation?></P>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
						  <label>คำนำหน้า</label>
						  <P><?=$btitle?></P>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>ชื่อ</label>
							<P><?=$bfname?></P>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>นามสกุล</label>
							<P><?=$blname?></P>
						</div>
					</div>
				</div>
				<? } ?>			
				<div class="content-header">
				  <div class="container">
					<div class="row mb-2">
					  <div class="col-sm-12">
						<h3>แผนประกัน</h3>
					  </div><!-- /.col -->
					</div><!-- /.row -->
				  </div><!-- /.container-fluid -->
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="card">
						  <div class="card-body">
							<h3><?=$pkg_name?></h3>
							<p class="lead">
							  <?=number_format($total)?> บาท
							</p>
							<p><B>ทุนประกันหลัก <?= number_format($sum_insure) ?> บาท</B></p>
						  </div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<h3>ความคุ้มครอง</h3>
						<table class="table table-bordered">
						  <thead>
							<tr>
							  <th>ข้อตกลงคุ้มครอง/เอกสารแนบท้าย</th>
							  <th>จำนวนเงินเอาประกันภัย</th>
							</tr>
						  </thead>
						  <tbody>
							 <?
							$sql = "SELECT A.*, B.* FROM scil.TXN_PACKAGE_COVERAGE A";
							$sql .= " LEFT JOIN scil.LTB_COVERAGE B";
							$sql .= " ON A.CID = B.CID";
							$sql .= " WHERE A.PKG_ID = '" . $pkg_id. "'";
							//echo $sql . "<BR>";
							$result = mysql_query($sql);
							while ($rs = mysql_fetch_array($result)) {
							?>
							<tr>
							  <td><?=$rs["COVERAGE_NAME"]?></td>
							  <td align="right"><?=number_format($rs["COVERAGE"])?></td>
							</tr>
						  <? } ?>
						  </tbody>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="offset-md-5 col-md-7">
						<a href="PA-COVID_list.php" class="btn btn-warning">&nbsp;&nbsp;&nbsp;กลับ&nbsp;&nbsp;&nbsp;</a>
					</div>
				</div>
				<!-- /.row -->
			  </div>
			</div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      
    </div>
    <strong>&copy; สงวนลิขสิทธิ์ 2021 บริษัท เอฟดับบลิวดีประกันภัย จำกัด (มหาชน)</strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="plugins/jszip/jszip.min.js"></script>
<script src="plugins/pdfmake/pdfmake.min.js"></script>
<script src="plugins/pdfmake/vfs_fonts.js"></script>
<script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
</body>
</html>
