<?
include("include/authen.php");
include("include/misc.php");

$conn = connect_db();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>FWD | PA & COVID-19</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-orange navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      <li class="nav-item">
        <div class="navbar-search-block">
          <form class="form-inline" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <div class="input-group input-group-sm">
              <input class="form-control form-control-navbar" type="search" name="Search" placeholder="Search" aria-label="Search">
              <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                  <i class="fas fa-search"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-warning elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
		<img src="images/fwd_logo.png" alt="FWD" class="brand-image img-circle elevation-3" style="opacity: .8">
		<span class="brand-text font-weight-light">Policy Online</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
		<div class="image">
			<a href="#" class="d-block"><i class="fas fa-user"></i></a>
		</div>
        <div class="info">
          <a href="#" class="d-block"><?=$_SESSION["ADMIN_NAME"]?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
		  <li class="nav-item menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-shield-alt"></i>
              <p>
                ประกันอุบัติเหตุและโควิค
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="PA-COVID_list.php" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>รายการกรมธรรม์</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="promocode_list.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>โปรโมชั่นโค้ด</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1>กรมธรรม์ประกันภัยอุบัติเหตุและการติดเชื้ออันเนื่องมาจากไวรัส COVID-19</h1>
		  </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>เลขที่กรมธรรม์</th>
                    <th>ชื่อ-นามสกุล</th>
                    <th>แผน</th>
                    <th>วันคุ้มครอง</th>
                    <th>สถานะ</th>
                  </tr>
                  </thead>
                  <tbody>
				  <?

          if($_POST['Search'] != '') {

            $sql = "SELECT * FROM scil_webservice.MAS_APPLICATION";
            $sql .= " WHERE CONCAT(UPPER(tempQuotationNo),UPPER(POLICY_ID),UPPER(tempFirstNm),UPPER(tempLastNm),UPPER(tempFormula),UPPER(tempExpiryDt),UPPER(tempIcCard)) LIKE UPPER('%".$_POST['Search']."%')";
            $sql .= " ORDER BY POLICY_TRANDATE DESC LIMIT 50";
            $result = mysql_query($sql);
          }
          else {
					  $sql = "SELECT * FROM scil_webservice.MAS_APPLICATION ";
					  $sql .= " WHERE tempFormula LIKE 'CO%'";
				 	  $sql .= " ORDER BY POLICY_TRANDATE DESC LIMIT 50";
					  $result = mysql_query($sql);
          }
					while ($rs = mysql_fetch_array($result)) {
				  ?>
                  <tr>
                    <td><a href="PA-COVID_detail.php?policy_id=<?=$rs["tempQuotationNo"]?>"><?=$rs["tempQuotationNo"]?></a></td>
                    <td><a href="PA-COVID_detail.php?policy_id=<?=$rs["tempQuotationNo"]?>"><?=$rs["tempTitleNm"]?> <?=$rs["tempFirstNm"]?> <?=$rs["tempLastNm"]?></a></td>
                    <td><?=$rs["tempFormula"]?></td>
                    <td><?=$rs["tempEffectDt"]?> - <?=$rs["tempExpiryDt"]?></td>
                    <td><? if ($rs["tempStatus"] == "1") { echo "คุ้มครอง"; } else { echo "ยกเลิก"; } ?></td>
                  </tr>
				  <? } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">

    </div>
    <strong>&copy; สงวนลิขสิทธิ์ 2021 บริษัท เอฟดับบลิวดีประกันภัย จำกัด (มหาชน)</strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="plugins/jszip/jszip.min.js"></script>
<script src="plugins/pdfmake/pdfmake.min.js"></script>
<script src="plugins/pdfmake/vfs_fonts.js"></script>
<script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
  });
</script>
</body>
</html>
